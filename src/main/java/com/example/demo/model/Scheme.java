package com.example.demo.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "scheme")
public class Scheme {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	

	@Column(name = "schemename")
	private String schemename;
	
	
	public String getSchemename() {
		return schemename;
	}

	public void setSchemename(String schemename) {
		this.schemename = schemename;
	}
	
	@Column(name = "fullname")
	private String fullname;
	
	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	@Column(name = "username")
	private String username;
	
	@Column(name = "verifiedby")
	private String verifiedby;
	
	@Column(name = "approvedby")
	private String approvedby;
	
	public String getVerifiedby() {
		return verifiedby;
	}

	public void setVerifiedby(String verifiedby) {
		this.verifiedby = verifiedby;
	}

	public String getApprovedby() {
		return approvedby;
	}

	public void setApprovedby(String approvedby) {
		this.approvedby = approvedby;
	}

	@Column(name = "address")
	private String address;
	
	@Column(name = "contact")
	private String contact;
	
	@Column(name = "aadhar")
	private long aadhar;
	
	@Column(name = "gender")
	private String gender;
	
	
	@Column(name = "dob")
	private String dob;
	
	@Column(name = "status")
	private String status;
	
	@Column(name="state")
	private String state;
	
	@Column(name="taluk")
	private String taluk;
	
	@Column(name="city")
	private String city;
	
	@Column(name="village")
	private String village;
	
	@Column(name="ration")
	private String ration;
	
	@Column(name="gram")
	private String gram;
	
	@Column(name="regno")
	private String regno;
	
	@Column(name="ageatreg")
	private String ageatreg;
	
	@Column(name="dateofreg")
	private String dateofreg;
	
	@Column(name="bankname")
	private String bankname;
	
	@Column(name="bankaccnumber")
	private String bankaccnumber;
	
	@Column(name="bankifsc")
	private String bankifsc;
	
	@Column(name="bankaddr")
	private String bankaddr;
	
	@Column(name="bankbranch")
	private String bankbranch;
	
	public Scheme() {
		
	}

	public Scheme(long id,String verifiedby,String approvedby,String fullname, String schemename, String username, String address, String contact, long aadhar,String gender, String status,String state,String dob, 
			String taluk, String city, String village, String ration, String gram, String regno, String ageatreg,
			String dateofreg, String bankname, String bankaccnumber, String bankifsc, String bankaddr,
			String bankbranch) {
		super();
		this.id = id;
		this.verifiedby=verifiedby;
		this.approvedby=approvedby;
		this.fullname=fullname;
		this.schemename=schemename;
		this.username = username;
		this.address = address;
		this.contact = contact;
		this.aadhar = aadhar;
		this.gender = gender;
		this.status = status;
		this.dob =dob;
		this.state=state;
		this.taluk = taluk;
		this.city = city;
		this.village = village;
		this.ration = ration;
		this.gram = gram;
		this.regno = regno;
		this.ageatreg = ageatreg;
		this.dateofreg = dateofreg;
		this.bankname = bankname;
		this.bankaccnumber = bankaccnumber;
		this.bankifsc = bankifsc;
		this.bankaddr = bankaddr;
		this.bankbranch = bankbranch;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public long getAadhar() {
		return aadhar;
	}

	public void setAadhar(long aadhar) {
		this.aadhar = aadhar;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTaluk() {
		return taluk;
	}

	public void setTaluk(String taluk) {
		this.taluk = taluk;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getRation() {
		return ration;
	}

	public void setRation(String ration) {
		this.ration = ration;
	}

	public String getGram() {
		return gram;
	}

	public void setGram(String gram) {
		this.gram = gram;
	}

	public String getRegno() {
		return regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}

	public String getAgeatreg() {
		return ageatreg;
	}

	public void setAgeatreg(String ageatreg) {
		this.ageatreg = ageatreg;
	}

	public String getDateofreg() {
		return dateofreg;
	}

	public void setDateofreg(String dateofreg) {
		this.dateofreg = dateofreg;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getBankaccnumber() {
		return bankaccnumber;
	}

	public void setBankaccnumber(String bankaccnumber) {
		this.bankaccnumber = bankaccnumber;
	}

	public String getBankifsc() {
		return bankifsc;
	}

	public void setBankifsc(String bankifsc) {
		this.bankifsc = bankifsc;
	}

	public String getBankaddr() {
		return bankaddr;
	}

	public void setBankaddr(String bankaddr) {
		this.bankaddr = bankaddr;
	}

	public String getBankbranch() {
		return bankbranch;
	}

	public void setBankbranch(String bankbranch) {
		this.bankbranch = bankbranch;
	}
	
}
	