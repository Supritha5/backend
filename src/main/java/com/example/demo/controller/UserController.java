package com.example.demo.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;


@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	// get all users
	@GetMapping("/users")
	public List<User> getAllUsers(){
		return userRepository.findAll();
	}		
	
	// create user rest api
	@PostMapping("/signup")
	public User createUser(@RequestBody User user) {
		return userRepository.save(user);
	}
	
	// get user by id rest api
	@GetMapping("/signin/id/{aadhar}")
	public ResponseEntity<User> getUserByAadhar(@PathVariable Long aadhar) {
		User user = userRepository.findByAadhar(aadhar);
				//.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + aadhar));
		return ResponseEntity.ok(user);
	}
	
	
	
	//@RequestMapping(value = "/signin/{username}", method = RequestMethod.GET)
	//@ResponseBody
	@GetMapping("/signin/username/{username}")
	public User findByUsername(@PathVariable String username) {
	    return userRepository.findByUsername(username);
	}
	// update user rest api
	
	@PutMapping("/users/{aadhar}")
	public ResponseEntity<User> updateUser(@PathVariable Long aadhar, @RequestBody User userDetails){
		User user = userRepository.findByAadhar(aadhar);
				//.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + aadhar));
		//user.setFullname(userDetails.getFullname());
		//user.setUsername(userDetails.getUsername());
		//user.setPassword(userDetails.getPassword());
		//user.setAddress(userDetails.getAddress());
		//user.setContact(userDetails.getContact());
		//user.setAadhar(userDetails.getAadhar());
		//user.setGender(userDetails.getGender());
		user.setStatus(userDetails.getStatus());
		user.setVerifiedby(userDetails.getVerifiedby());
		user.setApprovedby(userDetails.getApprovedby());
		User updatedUser = userRepository.save(user);
		return ResponseEntity.ok(updatedUser);
	}
	
	// delete user rest api
	@DeleteMapping("/users/{aadhar}")
	public ResponseEntity<Map<String, Boolean>> deleteUser(@PathVariable Long aadhar){
		User user = userRepository.findByAadhar(aadhar);
				//.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + aadhar));
		
		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
}