package com.example.demo.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Scheme;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.SchemeRepository;


@RestController
@RequestMapping("/api/v1/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class SchemeController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private SchemeRepository schemeRepository;
	
	// get all users
	@GetMapping("/form")
	public List<Scheme> getAllUsers(){
		return schemeRepository.findAll();
	}		
	
	// create user rest api
	@PostMapping("/submit")
	public Scheme createUser(@RequestBody Scheme scheme) {
		return schemeRepository.save(scheme);
	}
	
	// get user by id rest api
	@GetMapping("/form/id/{aadhar}")
	public ResponseEntity<Scheme> getUserByAadhar(@PathVariable Long aadhar) {
		Scheme scheme = schemeRepository.findByAadhar(aadhar);
				//.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + aadhar));
		return ResponseEntity.ok(scheme);
	}
	
	//@RequestMapping(value = "/signin/{username}", method = RequestMethod.GET)
	//@ResponseBody
	@GetMapping("/form/username/{username}")
	public User findByUsername(@PathVariable String username) {
	    return userRepository.findByUsername(username);
	}
	// update user rest api
	
	@PutMapping("/form/{aadhar}")
	public ResponseEntity<Scheme> updateScheme(@PathVariable Long aadhar, @RequestBody Scheme schemeDetails){
		Scheme scheme = schemeRepository.findByAadhar(aadhar);
				//.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + aadhar));
		scheme.setStatus(schemeDetails.getStatus());
		scheme.setVerifiedby(schemeDetails.getVerifiedby());
		scheme.setApprovedby(schemeDetails.getApprovedby());
		/*
		scheme.setFullname(schemeDetails.getFullname());
		scheme.setSchemename(schemeDetails.getSchemename());
		scheme.setUsername(schemeDetails.getUsername());
		scheme.setAddress(schemeDetails.getAddress());
		scheme.setContact(schemeDetails.getContact());
		scheme.setAadhar(schemeDetails.getAadhar());
		scheme.setGender(schemeDetails.getGender());
		scheme.setState(schemeDetails.getState());
		scheme.setTaluk(schemeDetails.getTaluk());
		scheme.setCity(schemeDetails.getCity());
		scheme.setVillage(schemeDetails.getVillage());
		scheme.setRation(schemeDetails.getRation());
		scheme.setGram(schemeDetails.getGram());
		scheme.setRegno(schemeDetails.getRegno());
		scheme.setAgeatreg(schemeDetails.getAgeatreg());
		scheme.setDateofreg(schemeDetails.getDateofreg());
		scheme.setBankname(schemeDetails.getBankname());
		scheme.setBankaccnumber(schemeDetails.getBankaccnumber());
		scheme.setBankifsc(schemeDetails.getBankifsc());
		scheme.setBankaddr(schemeDetails.getBankaddr());
		scheme.setBankbranch(schemeDetails.getBankbranch());
		*/
		Scheme updatedScheme = schemeRepository.save(scheme);
		return ResponseEntity.ok(updatedScheme);
	}
	
	// delete user rest api
	@DeleteMapping("/form/{aadhar}")
	public ResponseEntity<Map<String, Boolean>> deleteUser(@PathVariable Long aadhar){
		Scheme scheme = schemeRepository.findByAadhar(aadhar);
				//.orElseThrow(() -> new ResourceNotFoundException("User not exist with id :" + aadhar));
		
		schemeRepository.delete(scheme);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
}