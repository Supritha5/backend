package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewProj1Application {

	public static void main(String[] args) {
		SpringApplication.run(NewProj1Application.class, args);
	}

}
