--Create two tables users and scheme in order to populate initial values

--Create  Table schema
create table scheme (id  bigserial not null, aadhar int8, address varchar(255), ageatreg varchar(255), bankaccnumber varchar(255), bankaddr varchar(255), bankbranch varchar(255), bankifsc varchar(255), bankname varchar(255), city varchar(255), contact varchar(255), dateofreg varchar(255), dob varchar(255), gender varchar(255), gram varchar(255), ration varchar(255), regno varchar(255), schemename varchar(255), state varchar(255), status varchar(255), taluk varchar(255), username varchar(255), village varchar(255), primary key (id));

--Crete table users
create table users (id  bigserial not null, aadhar int8, address varchar(255), contact varchar(255), gender varchar(255), password varchar(255), status varchar(255), username varchar(255), primary key (id));

--To populate initial credentials
INSERT INTO public.users (id, aadhar, address, contact, gender, status, username, password) VALUES  (10001,15345652621, 'Bengaluru', '9978456123','Male','ver','Verifier','verifier123');
INSERT INTO public.users (id, aadhar, address, contact, gender, status, username, password) VALUES  (10002,15345652622, 'Bengaluru', '9874563214','Male','app','Approver','approver123');


--To display Table
SELECT * FROM users ORDER BY id ASC;

SELECT * FROM scheme ORDER BY id ASC;
